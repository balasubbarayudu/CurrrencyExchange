package com.hcl.ing.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.ing.dao.TransactionInfoDao;
import com.hcl.ing.model.TransactionInfo;

@Service
public class TransactionServiceImpl implements TransactionService {
	

	@Autowired
	TransactionInfoDao transactionInfoDao;
	public void saveTransactionDetails(TransactionInfo transactionInfo) {
		// TODO Auto-generated method stub
		transactionInfoDao.persistTransactioninfo(transactionInfo);
		
	}
	@Override
	public List<TransactionInfo> showTransactionDetails() {
		// TODO Auto-generated method stub
		return transactionInfoDao.getTransactionDetails();
	}
	

}
