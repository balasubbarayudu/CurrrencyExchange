package com.hcl.ing.service;

import java.util.List;

import com.hcl.ing.model.TransactionInfo;

public interface TransactionService {

 public void  saveTransactionDetails(TransactionInfo transactionInfo);
 public List<TransactionInfo> showTransactionDetails();
 
	
}
